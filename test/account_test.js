var chai = require('chai'),
Account = require('../src/account.js'),
expect = chai.expect;


//TESTS FOR ACCOUNT.JS//
describe("Account", function() {
  var accountName = 'alice'
  describe("constructor", function() {
    var user = new Account(2000, accountName);
    it("should initialize", function() {
      expect(user.constructor.name).to.equal("Account");
    });
  });
  describe("validate", function() {
    var user = new Account(2000, accountName);
    var valid = user.validate("5000", accountName);
    expect(valid).to.equal(false);
  });
  describe("retrieveBalance", function() {
    var user = new Account(2000, accountName);
    it("should be able to retrieve current Balance", function() {
      var balance = user.retrieveBalance(accountName);
      expect(balance).to.equal(2000);
    });
  });
  describe("editBalance", function() {
    it("should be able to change user's balance", function() {
      //CONFIRM EXPECTED BALANCE//
      var user = new Account(2000, accountName),
      balance = user.retrieveBalance( accountName);
      expect(balance).to.equal(2000);
      //CHANGE BALANCE AND RERUN ASSERTION//
      user.editBalance(accountName, 5000);
      balance = user.retrieveBalance(accountName);
      expect(balance).to.equal(5000);
    });
  });
});