var promptSchemas, colors;
colors = require('colors');

promptSchemas = {

  defaultSchema: {
    properties: {
      "default screen": {
        menu: "Transaction Menu:\n".blue +
              "enter 1 for login\n".blue +
              "enter 2 to deposit\n".blue +
              "enter 3 to withdraw\n".blue +
              "enter 4 to transfer\n".blue +
              "enter 5 to logout\n".blue,
        description: "enter choice 1-5".green,
        pattern: /^[1-5]$/,
        required: true
      }
    }
  },

  loginSchema: {
    properties: {
      "login": {
        description: "login".green,
        pattern: /^[a-zA-Z\s-]+$/,
        message: "nama must contain only text..".red,
        required: true
      },
    }
  },
  
  depositFunds: {
    properties: {
      "deposit funds": {
        description: "deposit :".green,
        pattern: /^[0-9]+\.[0-9][0-9]$/,
        message: "include to two decimal places".red,
        required: true
      }
    }
  },
  withdrawFunds: {
    properties: {
      "withdraw funds": {
        description: "withdraw:".green,
        pattern: /^[0-9]*[02468]0(\.00)?$/,
        message: "must be divisible by 20..".red,
        required: true
      }
    }
  },
  transferFunds: {
    properties: {
      "transfer funds": {
        description: "transfer :".green,
        pattern: /^[0-9]+\.[0-9][0-9]$/,
        message: "include to two decimal places".red,
        required: true
      },
      "transfer accountname": {
        description: "account name :".green,
        pattern: /^[a-zA-Z\s-]+$/,
        message: "nama must contain only text..".red,
        required: true
      }
    }
  },
};

module.exports = promptSchemas;