var Account, prompt, promptSchemas, colors, ATM;
Account = require('./account.js');
promptSchemas = require('./prompt_schemas.js');
prompt = require('prompt');
colors = require('colors');

ATM = (function() {
  function ATM() {
    
    var session, defaultScreenCallback,userRegistrationCallback,
       withdrawFundsCallback, depositFundsCallback, clearScreen;
    
    prompt.start();
    prompt.message = "";
    prompt.delimiter = " ";
    
    this.atmStatus = "ON";
    this.accounts = [];


    clearScreen = function() {
      process.stdout.write('\u001B[2J\u001B[0;0f');
    };

    defaultScreenCallback = function(err, choice) {
      clearScreen();
      if (err) { return; }
      if (choice["default screen"] === "1") {
        prompt.get( promptSchemas.loginSchema, userRegistrationCallback.bind(this) );
      }else if (choice["default screen"] === "2"){
        prompt.get( promptSchemas.depositFunds, depositFundsCallback.bind(this) );
      }else if (choice["default screen"] === "3"){
        prompt.get( promptSchemas.withdrawFunds, withdrawFundsCallback.bind(this) );
      }else if (choice["default screen"] === "4"){
        prompt.get( promptSchemas.transferFunds, transferFundsCallback.bind(this) );
      }else if (choice["default screen"] === "5"){
        logoutCallback.call(this);
      }
    };

    userRegistrationCallback = function(err, credentials) {
      clearScreen();
      if (err) { return; }
      var accountName = credentials["login"];

      if(this.accounts.length > 0){
        for(const i in this.accounts){
            if(accountName == this.accounts[i].accountName){
                session = this.accounts[i];
                var balance = this.checkBalance(),
                balanceString = "$" + balance.a.toString(10).red,
                owed = "$" + balance.b.toString(10).red
                accountTarget = balance.c.red;
                if(balance.b > 0){
                	console.log("Your balance is: ".blue, balanceString);
                	console.log("Owed: ".blue,  owed + " to " + accountTarget);
                }else{
                	console.log("Your balance is: ".blue, balanceString);
                }
                console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
                return prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
            }
        }
      }
      
      var initDeposit = parseFloat(0),
      accountNumber = this.newAccount(initDeposit, accountName),
      accountNumberString = accountNumber.toString(10).red;

      this.startSession( err, {"login": accountName, "account number": accountNumber}, true );
      console.log("\n");
      var balance = this.checkBalance(),
      balanceString = "$" + balance.a.toString(10).red;
      console.log("Your balance is: ".blue, balanceString);
    };

    depositFundsCallback = function (err, amount) {
      if (err) { return; }
      console.log("\n\n\n\n\n\n\n\n");
      var depositAmount =  parseFloat(amount["deposit funds"]),
      balance = this.depositFunds(depositAmount),
      balanceString = "$" + balance.toString(10).red;
      
      if(balance == "invalid session"){
        console.log("Sorry you don't login yet, please login first...");
      }else{
        console.log("Your new balance is: ".blue, balanceString);
      }
      
      console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
      prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
    };

    withdrawFundsCallback = function (err, amount) {
      if (err) { return; }
      console.log("\n\n\n\n\n\n\n\n");
      var withdrawalAmount = parseInt(amount["withdraw funds"], 10),
      balance = this.withdrawFunds(withdrawalAmount),
      balanceString = "$" + balance.toString(10).red;

      if(balance == "invalid session"){
        console.log("Sorry you don't login yet, please login first...");
      } else if (balance === "insufficient funds") {
        console.error("insufficient funds for requested transaction".red);
      }else{
        console.log("Your new balance is: ".blue, balanceString);
      }
      
      console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
      prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
    };

    transferFundsCallback = function (err, amount) {
      if (err) { return; }
      console.log("\n\n\n\n\n\n\n\n");
      var transferAmount =  parseFloat(amount["transfer funds"]),
      accountNameTarget = amount["transfer accountname"],
      balance = this.transferFunds(transferAmount, accountNameTarget);
      // balanceString = "$" + balance.toString(10).red;

      if(balance != "invalid session"){
      	if(balance.uO > 0){
      		var balanceString = "$" + balance.uB.toString(10).red;
      		var owed = "$" + balance.uO.toString(10).red;
      		var accountTarget = balance.uANT.red;
      		
      		console.log("Transferred: ".blue+ "$" +balance.transferoldBalance.toString(10).red+" to "+accountNameTarget);
        	console.log("Your new balance is: ".blue, balanceString);
        	console.log("Owed: ".blue,  owed + " to " + accountTarget);
      	}else{
      		var balanceString = "$" + balance.toString(10).red;
      		console.log("Transferred: ".blue+ "$" +transferAmount.toString(10).red+" to "+accountNameTarget);
        	console.log("Your new balance is: ".blue, balanceString);
      	}
      	
        
      }
      
      console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
      prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
    };

    logoutCallback = function (err, credentials) {
      if (err) { return; }

      if(session){

        console.log("Goodbye, ".blue, session.accountName,"!");
      }else{
        console.log("Sorry you don't login yet, please login first...");
      }

      console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
      prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
    };


    //CREATES SECURE NEW BANK ACCOUNTS//
    this.newAccount = function(initDeposit, accountName) {
      var newAccount = new Account(initDeposit, accountName);
      this.accounts.push(newAccount);
      
      //USER ACCOUNTS START AT ARBITARY 6 DIGIT NUMBER//
      newAccount.accountNumber = this.accounts.length + 195341;
      newAccount.accountName = accountName;
      return newAccount.accountNumber;
    };

    //STARTS BANKING SESSION BY VERIFYING USER//
    this.startSession = function(err, credentials, newUser) {
        if (err) { return; }
        var accountName = credentials["login"];
        var accountNumber = credentials["account number"];
        
        if(this.accounts[accountNumber - 195342] instanceof Account){
            var verified;
            verified = this.accounts[accountNumber - 195342].validate(accountName);
            if (verified) {
                session = this.accounts[accountNumber - 195342];
                this.atmStatus = "IN SESSION";
                
                if (newUser) {
                    console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
                    prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
                }
                
                return "session started";
            }
        }
    };

    this.on = function() {
      clearScreen();
      session = null;
      console.log(promptSchemas["defaultSchema"]["properties"]["default screen"]["menu"]);
      prompt.get( promptSchemas.defaultSchema, defaultScreenCallback.bind(this) );
      this.atmStatus = "LISTENING";
    };

    this.checkBalance = function() {
      if (session) {
        var balance = session.retrieveBalance(session.accountName);
        var a = balance.userBalance;
        var b = balance.userOwed;
        var c = balance.accountNameTarget;
        
        return {a,b,c};
      }
      return "invalid session";
    };

    this.withdrawFunds = function(amount) {
      if (session) {
        var newBalance,
        balance = this.checkBalance();
        if (balance.a >= amount) {
          newBalance = balance.a - amount;
          balance = session.editBalance(session.accountName, newBalance);
          return balance.userBalance;
        }
        
        return "insufficient funds";
      }
      return "invalid session";
    };

    this.depositFunds = function(amount) {
      if (session) {
        var newBalance,
        balance = this.checkBalance();
        newBalance = balance.a + amount;
        balance = session.editBalance(session.accountName, newBalance);
        return balance.userBalance;
      }
      return "invalid session";
    };

    this.transferFunds = function(amount, accountNameTarget) {
      if (session) {
        var newBalance, Balance_accountNameTarget, newBalance_accountNameTarget, finalBalance_accountNameTarget, owed,
        accountSender,accountSenderTemp,oldBalance,
        balance = this.checkBalance();
        
        newBalance = balance.a - amount;
        if(newBalance < 0){
            owed = Math.abs(newBalance);
            
        }
        accountSender = session.accountName;
        accountSenderTemp = [];

        if(this.accounts.length > 0){
            for(const i in this.accounts){
                
                if(accountNameTarget == this.accounts[i].accountName){
                    session = this.accounts[i];
                    Balance_accountNameTarget = session.retrieveBalance(accountNameTarget);
                    
                    if(newBalance < 0){
                        newBalance_accountNameTarget = Balance_accountNameTarget.userBalance + balance.a;
                        finalBalance_accountNameTarget = session.editBalance(accountNameTarget, newBalance_accountNameTarget, owed, 0,  accountSender);
                    }else{
                        newBalance_accountNameTarget = Balance_accountNameTarget.userBalance + amount;
                        finalBalance_accountNameTarget = session.editBalance(accountNameTarget, newBalance_accountNameTarget);
                    }
                    
                    
                    
                }
                if(accountSender == this.accounts[i].accountName){
                    accountSenderTemp.push(this.accounts[i]);
                    oldBalance = balance.a;
                }
                
            }
        }

        session = accountSenderTemp[0];
        if(newBalance < 0){
            newBalance = 0;
            balance = session.editBalance(session.accountName, newBalance, owed, oldBalance, accountNameTarget);
            
            var transferoldBalance = oldBalance;
            var uB = balance.userBalance;//new balance
            var uO = balance.userOwed;
            var uANT = balance.accountNameTarget;
            return {uB,uO,transferoldBalance,uANT};
        }else{
            balance = session.editBalance(session.accountName, newBalance);
            return balance.userBalance;
        }
        
      }
      return "invalid session";
    };
  }
  return ATM;
})();

module.exports = ATM;
