//ACCOUNT.JS//
//BANK ACCOUNT CONSTRUCTOR USED IN ATM.JS//
var Account = (function() {
      function Account(initDeposit, accountName) {
        //PRIVATE DATA//
        var userBalance, accountLedger, changeBalance,userOwed,userOldBalance,accountNameTarget,owedFrom,accounSender;
        userBalance = initDeposit;
        userOldBalance = initDeposit;
        userOwed = 0;
        accountNameTarget = "";
        owedFrom = 70;
        accounSender = "bob";

        //PRIVATE METHODS//

        changeBalance = function(newBalance) {
          userBalance = newBalance;
        };

        changeOwed = function(newOwed) {
          userOwed = newOwed;
        };

        changeOldBalance = function(OldBalance) {
          userOldBalance = OldBalance;
        };

        changeaccountNameTarget= function(NameTarget) {
          accountNameTarget = NameTarget;
        };

        //PUBLIC METHODS//

        this.validate = function(name) {
          if (name === accountName) {
            return true;
          }
          return false;
        };

        this.retrieveBalance = function(name) {
          if ( this.validate(name) ) {
            return {userBalance,userOwed,accountNameTarget,owedFrom,accounSender};
          }
          return "invalid credentials";
        };

        this.retrieveLedger = function(name) {
          if ( this.validate(name) ) {
            return accountLedger;
          }
          return "invalid credentials";
        };

        this.editBalance = function(name, newBalance, userOwed, userOldBalance, accountNameTarget) {
          if ( this.validate(name) ) {
            changeBalance(newBalance);
            if(userOwed){
              changeOwed(userOwed);
            }
            if(userOldBalance){
              changeOldBalance(userOldBalance);
            }
            if(accountNameTarget){
              changeaccountNameTarget(accountNameTarget);
            }
            return {userBalance,userOwed,userOldBalance,accountNameTarget};
          }
          return "invalid credentials";
        };
      }
      return Account;
    })();

module.exports = Account;
